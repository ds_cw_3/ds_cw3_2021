england_census
    URN:学校参考代码
        ·唯一
        ·000000-999999
    
    LA:Local Authority Number 地方当局编号

    ESTABlish:DfE number of establishment within LA 教育部在地方当局内的机构数量

    SCHOOLTYPE:学校类别

    NOR:在册学生总数

    NORG/NORB:在册学生女生数量/男生数量

    PNORG/PNORB:在册学生女生比例/男生比例

    TSENELSE:有特殊教育需要的学生中，有EHC计划的学生人数
        ·https://www.gov.uk/export-health-certificates
    
    PSENELSE:有特殊教育需要的学生中，有EHC计划的学生比例

    TSENELK:Number of eligible pupils with SEN support 有特殊教育需要支持的合格学生人数

    PSENELK:有特殊教育需要支持的合格学生百分比

    NUMEAL:No.  pupils where English not first language 英语不是第一语言的学生数量

    NUMENGFL:英语是第一语言的学生数量

    *NUMUNCFL:第一语言未分类的学生人数

    PNUMEAL:英语不是第一语言的学生百分比

    PNUMENGFL:英语是第一语言的学生百分比

    PNUMUNCFL:第一语言未分类的学生百分比

    NUMFSM:No. pupils eligible for free school meals 有资格享受免费校餐的学生人数

    NUMFSMEVER:过去6年内任何时候有资格FSM的学生人数。

    PNUMFSMEVER:过去6年内任何时候有资格FSM的学生百分比

england_ks4final:
    RECTYPE:Record type (1=mainstream school; 2=special school; 4=local authority; 5=National (all schools); 7=National (maintained schools))

    LEA:Local authority code

    ESTAB:Establishment number 机构数/编制数

    PCON_CODE:  Area Code

    PCON_NAME:

    CONTFLAG:Contingency flag - school results 'significantly affected'. This field is zero for all schools.

    ICLOSE:Closed school flag
    
    NFTYPE:School type
    
    RELDENOM:School religious character
    
    ADMPOL:School admissions policy (self-declared by schools on Edubase)
    
    *ADMPOL_PT:School admissions policy - new definition from 2018

    FEEDER:表示学校是否是六年级中心/联盟的附属学校（0=否；1=是）

    TABKS2:表示学校是否在小学（关键阶段2）成绩表中公布（0=否；1=是）

    TAB1618:表示学校是否在学校和学院(16-18)成绩表中公布(0=否；1=是)
    
    CONFEXAM:表示学校是否检查过成绩（R=否；空白=是）。
    
    TOTPUPS:所有年龄段在册学生的数量
    
    NUMBOYS:在册所有男生（包括part time）
    
    NUMGIRLS:在册所有女生（包括part time）

    TPUP:Number of pupils at the end of key stage 4

    BPUP:Number of boys at the end of key stage 4

    PBPUP:Percentage of pupils at the end of key stage 4 who are boys

    GPUP:Number of girls at the end of key stage 4

    PGPUP:Percentage of pupils at the end key stage 4 who are girls

    KS2APS:Key stage 2 Average Points Score of the cohort at the end of key stage 4 关键阶段二的学生在关键阶段四结束时的平均分数

england_school_information:
    URN:学校唯一代码

    LANAME:本地授权代码

    LAESTAB:本地授权ESTAB	

    SCHSTATUS:学校状态	

    MINORGROUP:Type of school / college eg maintained school

    SCHOOLTYPE:学校类别

    ISPRIMARY:Does the school provide primary education? ( 0 = No, 1 = Yes)

    ISSECONDARY:Does the school provide secondary education? ( 0 = No, 1 = Yes)

    ISPOST16:Does the school provide post 16 education? (  0 = No, 1 = Yes)

    RELCHAR:宗教

    ADMPOL:Admissions Policy 招生政策

england_abs:
    PERCTOT:占2018/19学年全年总缺勤率（经批准和未经批准）的百分比。

    PPERSABS10:持续旷课者的百分比 -- -- 在2018/19整个学年中缺席10%或以上的可能课程。

